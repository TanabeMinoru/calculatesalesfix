package jp.alhinc.calculate_sales;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class CalculateSalesFix {

	// 支店定義ファイル名
		private static final String FILE_NAME_BRANCH_LST = "branch.lst";
		// 支店別集計ファイル名
		private static final String FILE_NAME_BRANCH_OUT = "branch.out";

		// 支店定義ファイル名
		private static final String FILE_NAME_COMMODITY_LST = "commodity.lst";
		// 支店別集計ファイル名
		private static final String FILE_NAME_COMMODITY_OUT = "commodity.out";

		// エラーメッセージ
		private static final String UNKNOWN_ERROR = "予期せぬエラーが発生しました";
		private static final String FILE_NOT_EXIST = "定義ファイルが存在しません";
		private static final String FILE_INVALID_FORMAT = "定義ファイルのフォーマットが不正です";
		// 追加エラーメッセージ
		private static final String SALE_OVER_DIGITS = "合計金額が10桁を超えました";
		private static final String FILE_NAME_NOT_SERIAL = "売上ファイル名が連番になっていません";
		private static final String BRANCH_NOT_EXIST = "の支店コードが不正です";
		private static final String COMMODITY_NOT_EXIST = "の商品コードが不正です";
		private static final String INVALID_FORMAT = "のフォーマットが不正です";

		// 定義名
		private static final String BRANCH = "支店";
		private static final String COMMODITY = "商品";


	/**
	 * メインメソッド
	 *
	 * @param コマンドライン引数
	 */
	public static void main(String[] args) {

		// エラー処理(3-1)
		// 1. コマンドライン引数が渡されていない場合は、エラーメッセージ「予期せぬエラーが発生しました」を表示し、処理を終了する。
		if (args.length != 1) {
			System.out.println(UNKNOWN_ERROR);
			return;
		}

		// 支店コードと支店名を保持するMap
		Map<String, String> branchNames = new HashMap<>();
		// 支店コードと売上金額を保持するMap
		Map<String, Long> branchSales = new HashMap<>();

		// 支店定義ファイル読み込み処理
		if(!readFile(args[0], FILE_NAME_BRANCH_LST, branchNames, branchSales, BRANCH, "^[0-9]*$")) {
			return;
		}

		//商品定義の追加(処理4.1-2,1-3)
		// 商品コードと商品名を保持するMap
		Map<String, String> commodityNames = new HashMap<>();
		// 商品コードと売上金額を保持するMap
		Map<String, Long> commoditySales = new HashMap<>();
		// 商品定義ファイル読み込み処理
		if(!readFile(args[0], FILE_NAME_COMMODITY_LST, commodityNames, commoditySales, COMMODITY,"^[A-Za-z0-9]{8}$")) {
			return;
		}

		// ※ここから集計処理を作成してください。(処理内容2-1、2-2)
		File[] files = new File(args[0]).listFiles();
		List<File> rcdFiles = new ArrayList<>();

		// file一覧を参照
		for(int i = 0; i < files.length; i++){
			// ^[0-9]{8}
			if(files[i].isFile() && files[i].getName().matches("^\\d{8}.rcd$")){
				rcdFiles.add(files[i]);
			}
		}
		// エラー処理（2-1)
		// 売上ファイルが連番になっていない場合、エラーメッセージ「売上ファイル名が連番になっていません」を表示し、処理を終了する。
		for(int i = 0; i < rcdFiles.size() - 1; i++) {
			int former = Integer.parseInt(rcdFiles.get(i).getName().substring(0,8));
			int latter = Integer.parseInt(rcdFiles.get(i + 1).getName().substring(0,8));
			if((latter - former) != 1) {
			System.out.println(FILE_NAME_NOT_SERIAL);
			return;
			}
		}

		// 各支店集計ファイルを読み込み、集計する
		for(File file:rcdFiles) {
			BufferedReader br = null;

			//file.getName()をfildNameに値を代入
			String fildName = file.getName();

			try {
				FileReader fr = new FileReader(file);
				br = new BufferedReader(fr);

				String line = " ";
				// 一行ずつ読み込む
				ArrayList<String> saleData = new ArrayList<String>();
				while((line = br.readLine()) != null) {
				saleData.add(line);
				}

				//各支店集計ファイルの支店コード・商品コード・売上金額の値を代入
				String branchCode =  saleData.get(0);
				//商品定義の追加(処理4.2-2)
				String branchCommodity =  saleData.get(1);
				String branchMoney =  saleData.get(2);

				// エラー処理(2-4)
				// 4. 売上ファイルの中身が3行以上ある場合は、エラーメッセージ「<該当ファイル名>のフォーマットが不正です」と表示し処理を終了する。
				// 商品定義の追加(4.2-5)
				// 商品の追加に伴い、3桁以外はエラーとする
				if(saleData.size() != 3) {
					System.out.println( fildName+ INVALID_FORMAT);
					return;
				}

				// エラー処理(2-3)
				// 3. 支店に該当がなかった場合は、エラーメッセージ「<該当ファイル名>の支店コードが不正です」と表示し、処理を終了する。
				if (!branchNames.containsKey(branchCode)) {
					System.out.println( fildName + BRANCH_NOT_EXIST);
					return;
				}
				// 商品定義の追加(4.2-4)
				//商品コードが商品定義ファイルに該当しなかった場合は、エラーメッセージ「該当ファイル名の商品コードが不正です」と表示し、処理を終了する。
				if(!commodityNames.containsKey(branchCommodity)) {
					System.out.println(fildName + COMMODITY_NOT_EXIST);
					return;
				}


				// エラー処理(3-2)
				// 2. 売上ファイルの売上金額が数字ではなかった場合は、エラーメッセージ「予期せぬエラーが発生しました」を表示し、処理を終了する。
				if(!branchMoney.matches("^[0-9]*$")) {
					System.out.println(UNKNOWN_ERROR);
					return;
				}

				//商品定義の追加(4.2-2)
				// エラー処理(2-2)
				// 2. 合計金額が10桁を超えた場合、エラーメッセージ「合計金額が10桁を超えました」を表示し、処理を終了する。
				Long fileSales = Long.parseLong(branchMoney);
				Long SaleAmount = branchSales.get(branchCode) + fileSales;
				Long commoditySaleAmount = commoditySales.get(branchCommodity) + fileSales;
				if ((SaleAmount >= 10000000000L) || (commoditySaleAmount >= 10000000000L)){
					System.out.println(SALE_OVER_DIGITS);
					return;
				}
				branchSales.put(branchCode, SaleAmount);
			} catch(IOException e) {
				System.out.println(UNKNOWN_ERROR);
				return;

			} finally {
				// ファイルを開いている場合
				if(br != null) {
					try {
						// ファイルを閉じる
						br.close();
					} catch(IOException e) {
						System.out.println(UNKNOWN_ERROR);
						return;
					}
				}
			}
			return;
		}

		// 支店別集計ファイル書き込み処理
		if(!writeFile(args[0], FILE_NAME_BRANCH_OUT, branchNames, branchSales)) {
			return;
		}

		//商品定義の追加(処理4.1-3,1-4)
		// 商品別集計ファイル書き込み処理
		if(!writeFile(args[0], FILE_NAME_COMMODITY_OUT, commodityNames, commoditySales)) {
			return;
		}

	}

	/**
	 * 支店定義・商品定義ファイル読み込み処理
	 * @param string
	 * @param branch2
	 *
	 * @param フォルダパス
	 * @param ファイル名
	 * @param 支店コードと支店名を保持するMap
	 * @param 支店コードと売上金額を保持するMap
	 * @return 読み込み可否
	 */
		private static boolean readFile(String path, String fileName, Map<String, String> branchNames, Map<String, Long> branchSales, String definition, String regexp) {
		BufferedReader br = null;

		try {
			File file = new File(path, fileName);
			FileReader fr = new FileReader(file);
			br = new BufferedReader(fr);

			// エラー処理(1-1,1-3)
			// 支店定義ファイルが存在しない場合は、エラーメッセージ「支店定義ファイルが存在しません」を表示し、処理を終了する。
			//支店定義ファイルが存在しない場合は、エラーメッセージ「支店定義ファイルが存在しません」を表示し、処理を終了する。

			File checkFile = new File(path, fileName);
			if((!checkFile.exists())){
				System.out.println(definition + FILE_NOT_EXIST);
				return false;
			}

			// 一行ずつ読み込む
			String line;
			while((line = br.readLine()) != null) {
				// ※ここの読み込み処理を変更してください。(処理内容1-2)
				String[] items = line.split(",");

				// エラー処理（1-2,1-4）
				//支店定義ファイルのフォーマットが不正な場合は、エラーメッセージ「支店定義ファイルのフォーマットが不正です」を表示し、処理を終了する。
				//商品定義ファイルのフォーマットが不正な場合は、エラーメッセージ「商品定義ファイルのフォーマットが不正です」を表示し、処理を終了する。

				if((items.length != 2) || (items[0].matches(regexp))) {
					System.out.println(definition +  FILE_INVALID_FORMAT);
					return false;
				}
				branchNames.put(items[0], items[1]);
				branchSales.put(items[0], (long)0);
				System.out.println(line);
			}

		} catch(IOException e) {
			System.out.println(UNKNOWN_ERROR);
			return false;
		} finally {
			// ファイルを開いている場合
			if(br != null) {
				try {
					// ファイルを閉じる
					br.close();
				} catch(IOException e) {
					System.out.println(UNKNOWN_ERROR);
					return false;
				}
			}
		}
		return true;
	}

	/**
	 * 支店別集計ファイル・商品別集計ファイル書き込み処理
	 *
	 * @param フォルダパス
	 * @param ファイル名
	 * @param 支店コード（商品コード）と支店名（商品名）を保持するMap
	 * @param 支店コード（商品コード）と売上金額を保持するMap
	 * @return 書き込み可否
	 */
	private static boolean writeFile(String path, String fileName, Map<String, String> branchNames, Map<String, Long> branchSales) {
		// ※ここに書き込み処理を作成してください。(処理内容3-1)
		//支店別集計ファイルの作成
		File file = new File(path, fileName);
		BufferedWriter bw = null;

		try {
			FileWriter fw = new FileWriter(file);
			bw = new BufferedWriter(fw);

			//支店別集計ファイルへの出力内容を記述
			for(String key:  branchNames.keySet()) {
				// 入力形式
				// 支店コード,支店名,支店別合計
				bw.write(key + "," + branchNames.get(key) + "," + branchSales.get(key));
				bw.newLine();
			}

		} catch(IOException e) {
			System.out.println(UNKNOWN_ERROR);
		} finally {
			if(bw != null) {
				try {
					bw.close();
				} catch (IOException e) {
					System.out.println(UNKNOWN_ERROR);
				}
			}
		}
		return true;
	}
}
